public with sharing class DemoController {
    /**
     * An empty constructor for the testing
     */
    public DemoController() {
        // Initializing the custom object records list to store the Invoice Records created today
List<apex_invoice__c> PaidInvoiceNumberList = new List<apex_invoice__c>();

// SOQL query which will fetch the invoice records which has been created today
PaidInvoiceNumberList = [SELECT Id,Name, APEX_Status__c FROM APEX_Invoice__c WHERE
   CreatedDate = today];

// List to store the Invoice Number of Paid invoices
List<string> InvoiceNumberList = new List<string>();

// This loop will iterate on the List PaidInvoiceNumberList and will process each record
for (APEX_Invoice__c objInvoice: PaidInvoiceNumberList) {
   
   // Condition to check the current record in context values
   if (objInvoice.APEX_Status__c == 'Paid') {
      
      // current record on which loop is iterating
      System.debug('Value of Current Record on which Loop is iterating is'+objInvoice);
      
      // if Status value is paid then it will the invoice number into List of String
      InvoiceNumberList.add(objInvoice.Name);
   }
}

System.debug('Value of InvoiceNumberList '+InvoiceNumberList);
    }

    /**
     * Get the version of the SFDX demo app
     */
    public String getAppVersion() {
        return '1.0.0';
    }
}
